
# Succesful Relationships Analysis

Using Linear regression we can find the curves that best represents successful relationships



<br />

# <a name="motivation">1. Motivation</a>

The goal is to find the factors that are most likely going to leada to successful relationships such as age differnce in both genders , differnce in social class and differnce in marital status. this can be achieved by using machine learning techniques such as Linear Regression.


# <a name="results">2. Results </a>


![Age Difference ](images/age_diff.png)

![Social Class difference](images/class_diff.png)

![Best male marital status for divorced female with children](images/female_marital.png)

![Best female marital status for divorced male with children](images/male_marital.png)


